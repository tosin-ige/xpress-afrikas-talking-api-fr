import React,{component} from 'react';
import logo from './logo.svg';
import './App.css';
import LogoutComponent from './Slice/LogoutComponent';
import UserComponent from './Slice/UserComponent';
import ProfileComponent from './Slice/ProfileComponent';
import HomeComponent from './Slice/HomeComponent';
import {Route} from 'react-router';
import { HashRouter} from 'react-router-dom';
import AddUserComponent from './Slice/AddUserComponent';
import AddAPIComponent from './Slice/AddAPIComponent';
import ChangePasswordComponent from './Slice/ChangePasswordComponent';
import LoginComponent from './Slice/LoginComponent';
import LoaderComponent from './Slice/LoaderComponent';


function App() { 
  return (
    <div>
   <HashRouter>
    <Route exact path="/" component={HomeComponent}/>
    <Route path="/users" component={UserComponent}/>
    <Route path="/apis" component={ProfileComponent}/>
    <Route path="/Login" component={LoginComponent}/>
    <Route path="/Logout" component={LogoutComponent}/>
    <Route path="/addusers" component={AddUserComponent} />
    <Route path="/addservice" component={AddAPIComponent} />
    <Route path="/loader" component={LoaderComponent} />
    <Route path="/changepassword" component={ChangePasswordComponent} />
 </HashRouter>   
   </div>
  );
}

export default App;
