import React,{Component} from 'react';
import axios from 'axios';
import {NavLink, HashRouter,useHistory} from 'react-router-dom';
import AuthenticateComponent from './AuthenticateComponent';

export default class SidebarComponent extends Component{

  constructor(props)
  {
      super(props);
      };

    componentDidMount(){
//alert(sessionStorage.getItem("mysession"));      
    }

    render(){
        return(
            <div>



  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    
    <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style={{opacity: .8}} />
      <span class="brand-text font-weight-light">Xpress SMS</span>
    </a>

    
    <div class="sidebar">
      
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image" />
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

<HashRouter>
      
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
   
          </li>          
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                SMS Admin
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">2</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <NavLink to="/apis" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>SMS APIs</p>
                </NavLink>
              </li>
              <li class="nav-item">
                <NavLink to="/users" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Users</p>
                </NavLink>
              </li>

              <li class="nav-item">
                <NavLink to="/addusers" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User</p>
                </NavLink>
              </li>

              <li class="nav-item">
                <NavLink to="/Addservice" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Service</p>
                </NavLink>
              </li>
             
            </ul>
          </li>


          <li class="nav-item">
            <NavLink to="/changepassword" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Change Password
              </p>
            </NavLink>
          </li>

          <li class="nav-item">
            <NavLink to="/Logout" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Log Out
              </p>
            </NavLink>
          </li>
        


         
        </ul>
      </nav>
      </HashRouter>
      
    </div>
    
  </aside>

  
  


  
  

 


</div>
           
        )
    }
}