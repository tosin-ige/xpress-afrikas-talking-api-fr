import React,{Component} from 'react';
//import ProfileComponent from './ProfileComponent';
import axios from 'axios';
import {NavLink,HashRouter} from 'react-router-dom';
import SidebarComponent from './SidebarComponent';
import LoaderComponent from './LoaderComponent'

var url="";
export default class ProfileComponent extends Component{
   constructor(props){
       super(props); 
       this.state = {
        droplets: [],
        endpoint:'',
        provider:'',
        Id:'',
        status:'',
        isloading:true
      }
    
   }

   componentDidMount() {
    setInterval(() => {
        axios.post("user/authenticate")
        .then((response)=>{
            if(response.data==true)
            {
             // alert(response.data)
            }
            else
            {
               //  alert(response.data)
                 sessionStorage.clear();
                this.props.history.push(`/Login`)
            }
        })
        .catch((error)=>{
         // alert(error.data)
          console.log(error.data);
        })
         }, sessionStorage.getItem("mysession")) 
  


    axios.get(url+'/EndPoint/list')
        .then(droplets => {
            this.setState({ droplets: droplets.data,isloading:false });
        })
        .catch(function (error) {
            alert(error);
            this.setState({isloading:false})
        })
}

deleteProduct=(Id)=> {
    this.setState({isloading:true})
    axios.delete(url+"/EndPoint/"+Id)
    .then((res)=>{

        
        axios.get(url+'/EndPoint/list')
        .then(droplets => {
            this.setState({ droplets: droplets.data,isloading:false });
        })
        .catch(function (error) {
            alert(error.data);
            this.setState({isloading:false})
        })
        this.setState({isloading:false})
        alert("Delete was successful")

    })
    .catch((err)=>{
        alert(err)
        this.setState({isloading:false})
    })
  }

  HandleChange=(event)=>{
    let nam=event.target.name;
    let val=event.target.value;
    this.setState({[nam]:val});
}  
       
    handleSubmit1=(event)=>{   
      
event.preventDefault();

if(this.state.endpoint=="" || this.state.provider=="" )
{
  alert("*All Fields are Required!");
}
else
{
     axios.post(url+"/EndPoint/Add",
     {'endpoint':this.state.endpoint, 'provider':this.state.provider})
    .then((response) => {
        alert(response.data)
        this.setState({isloading:false})
      },(err)=>{
        alert(err);
        this.setState({isloading:false})
      });
    } 
           
  }

  handleEditDisplay=(i)=>{   
    this.setState({isloading:true})
   axios.get(url+"/EndPoint/"+i)
   .then((response)=>{
       this.setState({endpoint:response.data.endPoint,provider:response.data.provider,id:response.data.id,status:response.data.status,isloading:false})
       document.getElementById("myform").style.display = "block";
   })
   .then((err)=>{
      // alert(err)
       this.setState({isloading:false})
   })
   

  }

  handleEdit=(event)=>{ 
      if(this.state.endpoint==""||this.state.provider==""||this.state.status=="" ||this.state.id=="")
      {
          alert("All Fields are must be Filled!")
      }
      else
      {
        this.setState({isloading:true})
    axios.put(url+"/EndPoint/"+this.state.id,{endPoint:this.state.endpoint,provider:this.state.provider,id:this.state.id,status:this.state.status})
    .then((response)=>{       
        this.setState({endpoint:"",provider:"",id:"",status:""})
        // alert(response.data);// alert(response)
        axios.get(url+'/EndPoint/list')
        .then(droplets => {
            this.setState({ droplets: droplets.data,isloading:false });
        })
        document.getElementById("myform").style.display = "none";
    })
    .then((err)=>{
       // alert(err)
        this.setState({isloading:false})
    })
}
 event.preventDefault();
   }

    render(){   
        
        let content;
        if(this.state.isloading)
        {
         // content=<LoaderComponent />
         return <LoaderComponent />
        }
        else{

        return(      
            
             
          

            <div>
 <SidebarComponent />   

            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
              
              <HashRouter>
            <ul class="navbar-nav">
              <li class="nav-item">
                <NavLink class="nav-link" data-widget="pushmenu" to="/" role="button"><i class="fas fa-bars"></i></NavLink>
              </li>
              <li class="nav-item d-none d-sm-inline-block">
                <NavLink to="/apis" class="nav-link">SMS APIs</NavLink>
              </li>
              <li class="nav-item d-none d-sm-inline-block">
                <NavLink to="/users" class="nav-link">Users</NavLink>
              </li>
            </ul>
        </HashRouter>
            
            <form class="form-inline ml-3">
              <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" />
                <div class="input-group-append">
                  <button class="btn btn-navbar" type="submit" >
                    <i class="fas fa-search"></i>
                  </button>
                </div>
              </div>
            </form>
        
            
            <ul class="navbar-nav ml-auto">
              
              <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                  <i class="far fa-comments"></i>
                  <span class="badge badge-danger navbar-badge">0</span>
                </a>
               
              </li>
              
              <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                  <i class="far fa-bell"></i>
                  <span class="badge badge-warning navbar-badge">0</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                  <i class="fas fa-th-large"></i>
                </a>
              </li>
            </ul>
          </nav>





<div>       
  <div class="content-wrapper">
    
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <font class={{color:'########################################################################## LOG OUT WAS REMOVED FROM HERE ###############################################################'}}></font>
            </ol>
          </div>
        </div>
      </div>
    </div>
    
    




    <section class="content">
      <div class="container-fluid">
        
        
        
        
        
        <div class="row" style={{width: '100%'}}>
        
         

            <div class="col-lg-12 card">
             

              <div class="card-body" >
                <div class="tab-content p-0">
                  



                <form onSubmit = { this.handleEdit } method="post" style={{display: 'none'}} id="myform">  

                <div class="col-lg-6" >  

             <div class="form-group">
            <label> provider</label>
              <input type = "text" class="form-control" name = "provider" id="provider" value={this.state.provider} onChange={this.HandleChange}  placeholder="Enter Provider"/>
              </div>

              <div class="form-group">
            <label> Service/ Endpoint</label>
              <input type = "text" class="form-control" name = "endpoint" id="endpoint" value={this.state.endpoint} onChange={this.HandleChange} placeholder="Enter Service/ Endpoint Url"/>
            </div>

            <div class="form-group">
            <label> Status</label>
              <select class="form-control" name = "status" id="status" value={this.state.status} onChange={this.HandleChange}>
              <option value="">Select Status</option>
                  <option value="Active">Activate</option>
                  <option value="Dormant">De-activate</option>
              </select>
            </div>

            <button type = "submit" class="btn btn-primary"> Save Update </button>

            <input type = "text" class="form-control invisible" name = "id" id="id" value={this.state.id} onChange={this.HandleChange}  placeholder="ID" style={{ marginTop:"-150px;"}} disabled/>
                
          

</div>
          </form> 



                <table class="table table-hover table-bordered table-striped table-hover table-condensed">
      <thead class="thead-dark">
        <tr>
          <th>Id</th>
          <th>End Point</th>
          <th>Provider</th>
          <th>Status</th>
          <th>Date Modified</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      {this.state.droplets.map((data, i) => {
                            return (
                                <tr >
                                <td>{ data.id }</td>
                                <td>{ data.endPoint}</td>
                                <td>{ data.provider}</td>
                                <td>{ data.status }</td>
                                <td>{ data.lastModified }</td>
                                <button className="btn btn-success" onClick={() => this.handleEditDisplay(data.id)}>Edit Detail</button> &nbsp;
              <button className="btn btn-danger" onClick={() => this.deleteProduct(data.id)} >Delete</button>
                              </tr>
                            )
                        })}       
      </tbody>
    </table>

                   
                </div>
              </div>
            </div>

           

            </div>
           
            </div>
           
          </section>
         

          
          
      </div>



          <footer class="main-footer">
<strong>Copyright &copy; 2020 <a href="http://xpresspayments.com">Xpress Payments Solution LTD</a>.</strong>
All rights reserved.
<div class="float-right d-none d-sm-inline-block">
  <b>Version</b> 3.0.4
</div>
</footer>
<aside class="control-sidebar control-sidebar-dark">    
</aside> 
</div>


      



</div> 







        )
                    }
    }
}