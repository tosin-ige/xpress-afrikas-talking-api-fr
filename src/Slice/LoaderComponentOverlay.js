import LoadingOverlay from 'react-loading-overlay';

<LoadingOverlay
  active={isActive}
  spinner
  text='Loading your content...'
  >
  <p>Some content or children or something.</p>
</LoadingOverlay>