import React,{Component} from 'react';
import SidebarComponent from './SidebarComponent';
import axios from 'axios';
import {NavLink,HashRouter} from 'react-router-dom';
import LoaderComponent from './LoaderComponent';

export default class AddUserComponent extends Component{
   constructor(props){
       super(props);
this.state={
    currentpassword:'',
    newpassword:'',
    confirmnewpassword:'',
    isloading:true
}
   }

componentDidMount(){  
    axios.post("user/logout")
    .then((response)=>{
        if(response.data==true)
        {
            sessionStorage.clear();
            this.props.history.push(`/Login`)
        }
        else
        {
             alert(response.data)           
        }
    })
    .catch((error)=>{
      alert(error.data)
    })
}

    render(){   
      
      let content;
      if(this.state.isloading)
      {
       return <LoaderComponent />
      }
      else{

        return(      
            
                  
          

            <div>

<SidebarComponent />
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
              
              <HashRouter>
            <ul class="navbar-nav">
              <li class="nav-item">
                <NavLink class="nav-link" data-widget="pushmenu" to="/" role="button"><i class="fas fa-bars"></i></NavLink>
              </li>
              <li class="nav-item d-none d-sm-inline-block">
                <NavLink to="/apis" class="nav-link">SMS APIs</NavLink>
              </li>
              <li class="nav-item d-none d-sm-inline-block">
                <NavLink to="/users" class="nav-link">Users</NavLink>
              </li>
            </ul>
        </HashRouter>
            
            <form class="form-inline ml-3">
              <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" />
                <div class="input-group-append">
                  <button class="btn btn-navbar" type="submit" >
                    <i class="fas fa-search"></i>
                  </button>
                </div>
              </div>
            </form>
        
            
            <ul class="navbar-nav ml-auto">
              
              <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                  <i class="far fa-comments"></i>
                  <span class="badge badge-danger navbar-badge">0</span>
                </a>
               
              </li>
              
              <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                  <i class="far fa-bell"></i>
                  <span class="badge badge-warning navbar-badge">0</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                  <i class="fas fa-th-large"></i>
                </a>
              </li>
            </ul>
          </nav>





<div>       
  <div class="content-wrapper">
    
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <font class={{color:'########################################################################## LOG OUT WAS REMOVED FROM HERE ###############################################################'}}></font>
            </ol>
          </div>
        </div>
      </div>
    </div>
    
    




    <section class="content">
      <div class="container-fluid">
        
        
        
        
        
        <div class="row" style={{width: '100%'}}>
        
         

            <div class="col-lg-12 card">
             

              <div class="card-body" >
                <div class="tab-content p-0">
                  
  

                


                   
                </div>
              </div>
            </div>

           

            </div>
           
            </div>
           
          </section>
         

          
          
      </div>



          <footer class="main-footer">
<strong>Copyright &copy; 2020 <a href="http://xpresspayments.com">Xpress Payments Solution LTD</a>.</strong>
All rights reserved.
<div class="float-right d-none d-sm-inline-block">
  <b>Version</b> 3.0.4
</div>
</footer>
<aside class="control-sidebar control-sidebar-dark">    
</aside> 
</div>


      




</div> 







        )
      }
    }
}