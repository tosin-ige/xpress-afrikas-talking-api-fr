import React,{Component} from 'react';
//import ProfileComponent from './ProfileComponent';
import axios from 'axios';
import {NavLink,HashRouter} from 'react-router-dom';
import SidebarComponent from './SidebarComponent';
import LoaderComponent from './LoaderComponent';
import api from './api';

export default class ProfileComponent extends Component{
   constructor(props){
       super(props); 
       this.state = {
        droplets: [],
        surname:'',
    othername:'',
    phoneno:'',
    email:'',
    username:'',
    password:'',
    id:'',
    isloading:true
      }
   }

   componentDidMount() {

    setInterval(() => {
      axios.post("user/authenticate")
      .then((response)=>{
          if(response.data==true)
          {
           // alert(response.data)
          }
          else
          {
             //  alert(response.data)
               sessionStorage.clear();
              this.props.history.push(`/Login`)
          }
      })
      .catch((error)=>{
       // alert(error.data)
        alert(error.data);
      })
       }, sessionStorage.getItem("mysession")) 




    axios.get('/User/list')
        .then(droplets => {
         // alert(droplets)
            this.setState({ droplets: droplets.data,isloading:false });
        })
        .catch(function (error) {
            alert(error);
        })
}

HandleChange=(event)=>{
  let nam=event.target.name;
  let val=event.target.value;
  this.setState({[nam]:val});
}  

deleteProduct=(Id)=> {
  this.setState({isloading:true});
  axios.delete("/User/"+Id)
  .then((res)=>{


    axios.get('/User/list')
        .then(droplets => {
            this.setState({ droplets: droplets.data,isloading:false });
        })
        .catch(function (error) {
            alert(error);
        })
    this.setState({isloading:false});
    alert("Delete was successful")


  })
  .catch((err)=>{
    alert(err)
    this.setState({isloading:false});
  })
}

handleEditDisplay=(i)=>{   
  axios.get("/User/"+i)
  .then((response)=>{
      this.setState({surname:response.data.surname,othername:response.data.otheName,email:response.data.email,phoneno:response.data.phoneNo,username:response.data.userName,id:response.data.id,isloading:false})
      document.getElementById("myform1").style.display = "block"; 
    })
  .then((err)=>{
     // alert(err)
      this.setState({isloading:false});
  })
 }

 handleEdit=(event)=>{ 
  if(this.state.surname==""||this.state.othername==""||this.state.email=="" ||this.state.phoneno=="" ||this.state.username=="" ||this.state.id=="")
  {
      alert("All Fields are must be Filled!")
  }
  else
  {
   axios.put("/User/"+this.state.id,{surname:this.state.surname,othername:this.state.othername,email:this.state.email,phoneno:this.state.phoneno,userName:this.state.userName})
   .then((response)=>{       
       this.setState({surname:"",othername:"",id:"",email:"",phoneno:"",userName:""})
       
       axios.get('/User/list')
       .then(droplets => {
           this.setState({ droplets: droplets.data,isloading:false });
       })
       document.getElementById("myform1").style.display = "none";
   })
   .then((err)=>{
       //alert(err)
       this.setState({isloading:false});
   })
  }
event.preventDefault();
  }

    render(){  
      
      let content;
if(this.state.isloading)
{
 // content=<LoaderComponent />
 return <LoaderComponent />
}
else{

        return(    

            <div>




            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
              
              <HashRouter>
            <ul class="navbar-nav">
              <li class="nav-item">
                <NavLink class="nav-link" data-widget="pushmenu" to="/" role="button"><i class="fas fa-bars"></i></NavLink>
              </li>
              <li class="nav-item d-none d-sm-inline-block">
                <NavLink to="/apis" class="nav-link">SMS APIs</NavLink>
              </li>
              <li class="nav-item d-none d-sm-inline-block">
                <NavLink to="/users" class="nav-link">Users</NavLink>
              </li>
            </ul>
        </HashRouter>
            
            <form class="form-inline ml-3">
              <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" />
                <div class="input-group-append">
                  <button class="btn btn-navbar" type="submit" >
                    <i class="fas fa-search"></i>
                  </button>
                </div>
              </div>
            </form>
        
            
            <ul class="navbar-nav ml-auto">
              
              <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                  <i class="far fa-comments"></i>
                  <span class="badge badge-danger navbar-badge">0</span>
                </a>
               
              </li>
              
              <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                  <i class="far fa-bell"></i>
                  <span class="badge badge-warning navbar-badge">0</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                  <i class="fas fa-th-large"></i>
                </a>
              </li>
            </ul>
          </nav>





<div>       
  <div class="content-wrapper">
    
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <font class={{color:'########################################################################## LOG OUT WAS REMOVED FROM HERE ###############################################################'}}></font>
            </ol>
          </div>
        </div>
      </div>
    </div>
    
    




    <section class="content">
      <div class="container-fluid">
        
        
        
        
        
        <div class="row" style={{width: '100%'}}>
        
         

            <div class="col-lg-12 card">
             

              <div class="card-body" >
                <div class="tab-content p-0">
                  

                <form onSubmit = { this.handleEdit } method="post" style={{display:'none'}}   id="myform1" >  

<div class="col-lg-6"  >        

<div class="form-group">
<label> Surname</label>
<input type = "text" class="form-control" name = "surname" id="surname" value={this.state.surname} onChange={this.HandleChange}  placeholder="Enter Surname" />
</div>

<div class="form-group">
<label> Other Name</label>
<input type = "text" class="form-control" name = "othername" id="othername" value={this.state.othername} onChange={this.HandleChange} placeholder="Enter Name"/>
</div>

<div class="form-group">
<label> Email</label>
<input type = "email" class="form-control" name = "email" id="email" value={this.state.email} onChange={this.HandleChange}  placeholder="Email Address"/>
</div>




<div class="form-group">
<label> Phone No</label>
<input type = "text" class="form-control" name = "phoneno" id="phoneno" value={this.state.phoneno} onChange={this.HandleChange} placeholder="Enter Contact No"/>
</div>

<div class="form-group">
<label> User Name</label>
<input type = "text" class="form-control" name = "username" id="username" value={this.state.username} onChange={this.HandleChange} placeholder="Preferred User Name" disabled/>
</div>

<button type = "submit" class="btn btn-primary"> Save Update </button>

<input type = "text" class="form-control invisible" name = "id" id="id" value={this.state.id} onChange={this.HandleChange}  placeholder="ID" />
                
</div>
</form> 



                <table class="table table-hover table-bordered table-striped table-condensed">
      <thead class="thead-dark">
        <tr>
          <th>Id</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>User Name</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      {this.state.droplets.map((data, i) => {
                            return (
                                <tr >
                                <td>{ data.id }</td>
                                <td>{ data.otheName }</td>
                                <td>{ data.surname }</td>
                                <td>{ data.email}</td>
                                <td>{ data.phoneNo }</td>
                                <td>{ data.userName }</td>
                                <td>
              <button className="btn btn-success" onClick={() => this.handleEditDisplay(data.id)}>Edit Detail</button> &nbsp;
              <button className="btn btn-danger" onClick={() => this.deleteProduct(data.id)} >Delete</button>
            </td>
                              </tr>
                            )
                        })}       
      </tbody>
    </table>

                   
                </div>
              </div>
            </div>

           

            </div>
           
            </div>
           
          </section>
         

          
          
      </div>



          <footer class="main-footer">
<strong>Copyright &copy; 2020 <a href="http://xpresspayments.com">Xpress Payments Solution LTD</a>.</strong>
All rights reserved.
<div class="float-right d-none d-sm-inline-block">
  <b>Version</b> 3.0.4
</div>
</footer>
<aside class="control-sidebar control-sidebar-dark">    
</aside> 
</div>


<SidebarComponent />


</div> 







        )
                      }



    }
}