import React,{Component} from 'react';
import axios from 'axios';
import localStorage from 'local-storage';
import './Login1/vendor/fontawesome-free/css/all.min.css';
import './Login1/css/sb-admin-2.min.css';
import logo from './Images/xpress.gif';


var url="";
export default class LoginComponent extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            username:'',
            password:''
        };
    }
    
    HandleChange=(event)=>{
        const nam=event.target.name;
        const value=event.target.value;
        this.setState({[nam]:value});
    }
    HandleSubmit=(event)=>{
event.preventDefault();
if(this.state.username==="")
{
    alert("Enter Username!");
}
else if(this.state.password==="")
{
    alert("Enter Password!");
}
else
{
axios.post(url+"User/auth",{"username":this.state.username,"password":this.state.password })
.then((result)=>{
    if(result.data=="Success"){
      sessionStorage.setItem("mysession", 1920000);
        this.props.history.push(`/`) 
      }
    else{ alert(result.data);  
}
})
.catch((error)=>{
    console.log(error.data)
   // alert(error.data)
})
}
    }
    render(){
        return(

<div class="bg-gradient-primary">



            <div class="container">

   
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
           
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block"  >      <img src={logo} style={{height:400}} width={500} />       </div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">SMS Admin Portal!</h1>
                  </div>
                  <form class="user" method="post">
                    <div class="form-group">
                      <input type="text" name="username" class="form-control form-control-user"  onChange={this.HandleChange} id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Your UserName" required/>
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" class="form-control form-control-user"  onChange={this.HandleChange} id="exampleInputPassword" placeholder="Password" required/>
                    </div>
                    
                    <input type="button"  value="Login" onClick={this.HandleSubmit} class="btn btn-primary btn-user btn-block" />
                   
                     
                    <hr/>
                    
                  </form>
                  <hr/>
                  <div class="text-center">
                    <a class="small" href="#">Forgot Password?</a>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

  </div>
        
        );
    }

}

