import React,{Component} from 'react';
import {NavLink,HashRouter,Route} from 'react-router-dom';
import SidebarComponent from './SidebarComponent'

export default class HomeComponent extends Component{
    render(){
        return(
          <div>
<SidebarComponent />

            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
              
              <HashRouter>
            <ul class="navbar-nav">
              <li class="nav-item">
                <NavLink class="nav-link" data-widget="pushmenu" to="/" role="button"><i class="fas fa-bars"></i></NavLink>
              </li>
              <li class="nav-item d-none d-sm-inline-block">
                <NavLink to="/apis" class="nav-link">SMS APIs</NavLink>
              </li>
              <li class="nav-item d-none d-sm-inline-block">
                <NavLink to="users" class="nav-link">Users</NavLink>
              </li>
            </ul>
        </HashRouter>
            
            <form class="form-inline ml-3">
              <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" />
                <div class="input-group-append">
                  <button class="btn btn-navbar" type="submit" >
                    <i class="fas fa-search"></i>
                  </button>
                </div>
              </div>
            </form>
        
            
            <ul class="navbar-nav ml-auto">
              
              <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                  <i class="far fa-comments"></i>
                  <span class="badge badge-danger navbar-badge">0</span>
                </a>
               
              </li>
              
              <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                  <i class="far fa-bell"></i>
                  <span class="badge badge-warning navbar-badge">0</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                  <i class="fas fa-th-large"></i>
                </a>
              </li>
            </ul>
          </nav>










<div>       
  <div class="content-wrapper">
    
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <font class={{color:'########################################################################## LOG OUT WAS REMOVED FROM HERE ###############################################################'}}></font>
            </ol>
          </div>
        </div>
      </div>
    </div>
    
    
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <div class="col-lg-3 col-6">
            
            <div class="small-box bg-info">
              <div class="inner">
                <h3>SMS APIs</h3>

                <p>Report View</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          
          <div class="col-lg-3 col-6">
            
            <div class="small-box bg-success">
              <div class="inner">
                <h3>Users<sup style={{fontsize: 20}}>%</sup></h3>

                <p>Report View</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          
          <div class="col-lg-3 col-6">
            
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>Password</h3>

                <p>Admin Task</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          
          <div class="col-lg-3 col-6">
            
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>Log Out</h3>

                <p>Xpress SMS</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
      
        </div>
        
        
        <div class="row">
        
          <section class="col-lg-7 connectedSortable">
            
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-chart-pie mr-1"></i>
                  Sales
                </h3>
                
              </div>
              <div class="card-body">
                <div class="tab-content p-0">
                  
                  <div class="chart tab-pane active" id="revenue-chart"
                       style={{position: 'relative', height: 300}}>
                      <canvas id="revenue-chart-canvas" height="300" style={{height: 300}}></canvas>                         
                   </div>
                  <div class="chart tab-pane" id="sales-chart" style={{position: 'relative', height: 300}}>
                    <canvas id="sales-chart-canvas" height="300" style={{height: 300}}></canvas>                         
                  </div>  
                </div>
              </div>
            </div>
            </section>
            </div>
           
            </div>
           
          </section>
         

          <section class="col-lg-5 connectedSortable">          
          </section>
          
      </div>

          <footer class="main-footer">
<strong>Copyright &copy; 2020 <a href="http://xpresspayments.com">Xpress Payments Solution LTD</a>.</strong>
All rights reserved.
<div class="float-right d-none d-sm-inline-block">
  <b>Version</b> 3.0.4
</div>
</footer>
<aside class="control-sidebar control-sidebar-dark">    
</aside> 
</div>


      




</div>        
        
        )
    }
}